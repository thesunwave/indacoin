module.exports = function(grunt) {
  var build = "./build"
  grunt.initConfig({
    watch: {
      main: {
        files: ['src/**/*'],
        tasks: ['clean', 'jade:main', 'copy:css', 'copy:js', 'copy:pic']
      }
    },

    clean: [build],

    jade: {
      main: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [
          {
            expand: true,
            cwd: "src/jade/",
            src: ["*.jade"],
            dest: build,
            ext: ".html"
          }
        ]
      }
    },

    copy: {

      css : {
        files: [
          {
            expand: true,
            cwd: "src/css/",
            src: ["*.css"],
            dest: build + "/css",
            ext: ".css"
          }
        ]
      },

      js : {
        files: [
          {
            expand:true,
            cwd: "src/js/",
            src: ["*.js", "*.*.js"],
            dest: build + "/js",
            ext: ".js"
          }
        ]
      },

      pic : {
        files: [
          {
            expand: true,
            cwd: "./img",
            src: ["*.jpg"],
            dest: build + "/img",
            ext: ".jpg"
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-jade");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask("build", ["clean","jade:main", "copy:css", "copy:js", "copy:pic"]);
  grunt.registerTask("default", ["watch"]);
};
